class MyBigNumber:
  
  def sum(self, stn1, stn2):
    arr1 = []
    arr2 = []
    result = ""
    tmp = 0

    a = len(stn1)
    b = len(stn2)

    if a > b:
      stn2 = stn2.zfill(a)
    else:
      stn1 = stn1.zfill(b)

    for i in stn1:
      arr1.append(int(i))
    
    for j in stn2:
      arr2.append(int(j))

    arr1.reverse()
    arr2.reverse()

    for i in range(0, len(stn1)):
      if (i == len(stn1)):
        c = arr1[i] + arr2[i] + self.tmp
        c = str(c)
        result =  c + result
        tmp = 0
        print(result)
        break
      
      c = arr1[i] + arr2[i] + tmp
      if (c >= 10):
        c = str(c)
        result = c[1] + result 
        tmp = 1
      else:
        c = str(c)
        result =  c + result
        tmp = 0
      
      print(result)

    return result

if __name__ == "__main__":

    m = MyBigNumber()

    print(m.sum("10254", "16"))